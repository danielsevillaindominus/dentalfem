from flask import Flask, render_template, request, flash, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
import random
import datetime
from sqlalchemy.sql.functions import user
from datetime import date, time, datetime
import logging
from sqlalchemy import select
from flask_mail import Mail, Message
import base64
from PIL import Image
import io
import zipfile
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED
app = Flask(__name__)

#DATA BASE CONECTION
from flask_cors import CORS



app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://daniel.sevilla:Indominus_2021*;@localhost/dentalfemdb'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'secret string'
db = SQLAlchemy(app)

mail_config = {
	"MAIL_SERVER": "smtp.ionos.es",
	"MAIL_PORT": 587,
	"MAIL_USE_TLS": True,
	"MAIL_USE_SSL" : False,
	"MAIL_USERNAME": "daniel.sevilla@indominus.eu", # enviar correo electrónico
	"MAIL_PASSWORD": "Indominus_2021*;"# Código de autorización del cliente
}

app.config.update(mail_config)
 
mail = Mail(app)


#CLASS THAT LOADS SIMULATIONS
class Dicoms(db.Model):

    __tablename__ = 'dicoms'
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    content = db.Column(db.LargeBinary)
    

    def __init__(self, id, name, content):
        self.id = id
        self.name = name
        self.content = content
        
@app.errorhandler(Exception)          
def basic_error(e):          
    return "an error occured: " + str(e)


#----------------------------LOAD DICOM---------------------------------------------------------

@app.route('/loadDicomDB')

def loadDicomDB():

		zf = zipfile.ZipFile("dicom.zip", mode="w")
		try:
		    zf.write("dicom.dcm", compress_type=compression)
		finally:
		    zf.close()


		id_dicom = "006"
		name_dicom = "test"
		dicomFile = open('dicom.zip','rb').read()
		dicomFileEncode = base64.b64encode(dicomFile)
		
		record = Dicoms(id_dicom, name_dicom, dicomFileEncode)
		db.session.add(record)
		db.session.commit()
		
		return "ok"
		

	
		
#----------------------------GET DICOM---------------------------------------------------------

@app.route('/getDicomDB', methods=['GET', 'POST'])

def getDicomDB():

	
	
		
		dicom_query = Dicoms.query.filter_by(id='001').first()
		binary_data = base64.b64decode(dicom_query.content)
		image = Image.open(io.BytesIO(binary_data))
		image.show()
		
		response = binary_data
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response

	

