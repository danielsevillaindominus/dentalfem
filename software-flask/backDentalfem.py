from flask import Flask, render_template, request, flash, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
import random
import datetime
from sqlalchemy.sql.functions import user
from datetime import date, time, datetime
import logging
from sqlalchemy import select
from flask_mail import Mail, Message
import urllib.request as requestLib
import base64
from base64 import b64encode
from io import StringIO
from werkzeug.utils import secure_filename
import os
import sys
from PIL import Image
import io
from io import BytesIO
import time

app = Flask(__name__)

#COLORES
#https://javier.xyz/cohesive-colors/?src=594F4F,547980,45ADA8,9DE0AD,E5FCC2&overlay=FF9C00&intensity=0

#DATA BASE CONECTION
from flask_cors import CORS



app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://dentalfemdb:14231423@localhost/dentalfemdb'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'secret string'
db = SQLAlchemy(app)

mail_config = {
	"MAIL_SERVER": "smtp.ionos.es",
	"MAIL_PORT": 587,
	"MAIL_USE_TLS": True,
	"MAIL_USE_SSL" : False,
	"MAIL_USERNAME": "dentalfem@indominus.eu", # enviar correo electrónico
	"MAIL_PASSWORD": "*DentalFEM_2022"# Código de autorización del cliente
}

app.config.update(mail_config)
 
mail = Mail(app)

  



#CLASS THAT LOADS SIMULATIONS
class Simulations(db.Model):
    __tablename__ = 'simulations'
    id_simulation = db.Column(db.String, primary_key=True)
    file_simulation = db.Column(db.Integer)
    file_model = db.Column(db.Integer)
    completed = db.Column(db.Boolean, default=False, nullable=False)
    id_user = db.Column(db.String)
    id_patient = db.Column(db.String)
    

    def __init__(self, id_simulation, file_simulation, file_model, completed, id_user, id_patient):
        self.id_simulation = id_simulation
        self.file_simulation = file_simulation
        self.file_model = file_model
        self.completed = completed
        self.id_user = id_user
        self.id_patient = id_patient

#CLASS THAT LOADS USERS
class Users(db.Model):
    __tablename__ = 'users'
    email = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    credits = db.Column(db.Integer)
    security_token = db.Column(db.String)
    user_dicom_id = db.Column(db.String)
    
    def __init__(self, email, password, credits, security_token, user_dicom_id):
        self.email = email
        self.password = password
        self.credits = credits
        self.security_token = security_token
        self.user_dicom_id = user_dicom_id

#CLASS THAT LOADS PATIENTS
class Patients(db.Model):
	__tablename__ = 'patients'
	id_patient = db.Column(db.String, primary_key=True)
	name = db.Column(db.String)
	id_user = db.Column(db.String)
	birth = db.Column(db.DateTime)
	last_checking = db.Column(db.DateTime)
	last_name = db.Column(db.String)
	number_visits = db.Column(db.Integer) 
	genre = db.Column(db.Boolean, default=False, nullable=False)
	address = db.Column(db.String)
	first_visit = db.Column(db.DateTime)
	notes = db.Column(db.String)
	contact = db.Column(db.String)
	image_ask = db.Column(db.Boolean, default=False, nullable=False)
	image_url = db.Column(db.LargeBinary)


	def __init__(self, id_patient, name, id_user, birth, last_checking, last_name, number_visits, genre, address, first_visit, notes, contact, image_ask, image_url):
		self.id_patient = id_patient
		self.name = name
		self.id_user = id_user
		self.birth = birth
		self.last_checking = last_checking
		self.last_name = last_name
		self.number_visits = number_visits
		self.genre = genre
		self.address = address
		self.first_visit = first_visit
		self.notes = notes
		self.contact = contact
		self.image_ask = image_ask
		self.image_url = image_url

#CLASS THAT LOADS PREMIUM
class Premium(db.Model):
    __tablename__ = 'premium'
    id_user = db.Column(db.String, primary_key=True)
    

    def __init__(self, id_user):
        self.id_user = id_user

#CLASS THAT LOADS pdfs
class Pdffiles(db.Model):
    __tablename__ = 'pdfFiles'
    id_simulation = db.Column(db.String, primary_key=True)
    datecreated = db.Column(db.String)
    id_patients = db.Column(db.String)
    

    def __init__(self, id_simulation, datecreated, id_patients):
        self.id_simulation = id_simulation
        self.datecreated = datecreated
        self.id_patients = id_patients
        
#CLASS THAT LOADS requests
class Requests(db.Model):
    __tablename__ = 'requests'
    id_user_request = db.Column(db.String, primary_key=True)
    body = db.Column(db.String)
    date_sended = db.Column(db.DateTime)
    checked = db.Column(db.Boolean, default=False, nullable=False)
    
    def __init__(self, id_user_request, body, date_sended, checked):
        self.id_user_request = id_user_request
        self.body = body
        self.date_sended = date_sended
        self.checked = checked
        
#CLASS THAT LOADS notification
class Notification(db.Model):
    __tablename__ = 'notification'
    id_user_request = db.Column(db.String, primary_key=True)
    id_simulation = db.Column(db.String)
    finished = db.Column(db.Boolean, default=False, nullable=False)
    date = db.Column(db.DateTime)
    readed = db.Column(db.Boolean, default=False, nullable=False)
    id_notification = db.Column(db.String, primary_key=True)
    
    def __init__(self, id_user_request, id_simulation, finished, date, readed, id_notification):
        self.id_user_request = id_user_request
        self.id_simulation = id_simulation
        self.finished = finished
        self.date = date
        self.readed = readed
        self.id_notification = id_notification

class Samples(db.Model):

    __tablename__ = 'samples'
    id_sample = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    date = db.Column(db.DateTime)
    

    def __init__(self, id_sample, name, content, date):
        self.id_sample = id_sample
        self.name = name
        self.date = date


#---------------------REGISTER-------------------------------------------------------------
#http://127.0.0.1:5000/register?email=daniel@gmail.com&password=1423
@app.route('/signin', methods=['GET', 'POST'])

def signin():

	if request.args.get('password')==request.args.get('new_password'):
		

		email = request.args.get('email')
		password = request.args.get('password')
		credits = 0
		security_token = random.randint(0, 99999999999999)
		
		
		
		# GENERATE KEY FOR USERS
		
		id_user_pre = len(Users.query.all())
		id_user = id_user_pre + 1;
		
		if ( len(str(id_user))==3 ):
			id_user = str(id_user);
		if(len(str(id_user))==2):
			id_user = "0" + str(id_user);
		if(len(str(id_user))==1):
			id_user = "00" + str(id_user);
		
		

		record = Users(email, password, credits, security_token, id_user)
		db.session.add(record)
		db.session.commit()

		#

		response = jsonify(security_token, 0, email)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
		
		

	else:
		response = jsonify("USED EMAIL", 1)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response

#---------------------SIGNIN-------------------------------------------------------------    		
    		
@app.route('/login', methods=['GET', 'POST'])
def login():


	try:

		emailPost = request.args.get('email')
		passwordPost = request.args.get('password')
		security_token = random.randint(0, 99999999999999)

    
		if db.session.query(db.exists().where(Users.email == emailPost)).scalar(): #QUERY EMAIL & PASSWORD
			
			
			
			
			
			
			userloged = Users.query.filter_by(email=emailPost).first()
			if userloged.security_token == '0':
			
			
				if userloged.password == passwordPost:
					
					secretNumber = random.randint(0, 99999999999999)
					userloged.security_token = secretNumber
					db.session.merge(userloged)
					db.session.commit()
					
					#IMPORTANT
					response = jsonify(secretNumber, 0, emailPost)
					response.headers.add("Access-Control-Allow-Origin", "*")
					return response
					
				else:
					
					response = jsonify("WRONG EMAIL, WRONG PASSWORD", 1)
					response.headers.add("Access-Control-Allow-Origin", "*")
					return response
			else:
			
			
				if userloged.password == passwordPost:
						
					secretNumber = random.randint(0, 99999999999999)
					userloged.security_token = secretNumber
					db.session.merge(userloged)
					db.session.commit()
					
					#IMPORTANT
					response = jsonify(secretNumber, 4, emailPost)
					response.headers.add("Access-Control-Allow-Origin", "*")
					return response
				
	
		else:
			
			response = jsonify("NO RUNNING", 2)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response		
	except Exception:
	
		response = jsonify("ERROR", 2)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response



#---------------------GET ALL SIMULATIONS-------------------------------------------------------------    		
    		
@app.route('/getSimulations', methods=['GET', 'POST'])
def getSimulations():

	try:
		token_request = request.args.get('token')

		if db.session.query(db.exists().where(Users.security_token==token_request)).scalar(): #QUERY TOKEN

			userData = Users.query.filter_by(security_token=request.args.get('token')).first()
			allSimulations = Simulations.query.filter_by(id_user=userData.email).all()
			all_simulations = [{'id_simulation':simulation.id_simulation, 'id_patient':simulation.id_patient, 'file_model':simulation.file_model} for simulation in allSimulations]
			response = jsonify(all_simulations, 0)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			
		else:
			response = jsonify("GET SIMULATIONS ERROR", 1)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			
	except Exception:	
		

		response = jsonify("ERROR", 2)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
		
#---------------------GET ALL PATIENTS-------------------------------------------------------------    		
    		
@app.route('/getPatients', methods=['GET', 'POST'])
def getPatients():

	
	token_request = request.args.get('token')

	if db.session.query(db.exists().where(Users.security_token==token_request)).scalar(): #QUERY TOKEN

		userData = Users.query.filter_by(security_token=request.args.get('token')).first()
		allPatients = Patients.query.filter_by(id_user=userData.email).all()
		
		
		
		all_patients = [{'id_patient':patient.id_patient, 'name':patient.name, 'birth':patient.birth, 'last_checking':patient.last_checking, 'last_name':patient.last_name, 'number_visits':patient.number_visits, 'genre':patient.genre, 'address':patient.address, 'first_visit':patient.first_visit, 'notes':patient.notes, 'contact':patient.contact, 'peridontal':patient.image_ask} for patient in allPatients]
		
		
		
		response = jsonify(all_patients, 0)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
		
	else:
		response = jsonify("GET PATIENTS ERROR", 1)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response

#---------------------GET ALL NOTIFICATIONS-------------------------------------------------------------    		

			
			
@app.route('/getNotifications', methods=['GET', 'POST'])
def getNotifications():

	
	token_request = request.args.get('token')

	if db.session.query(db.exists().where(Users.security_token==token_request)).scalar(): #QUERY TOKEN

		userData = Users.query.filter_by(security_token=request.args.get('token')).first()
		
		allNotifications = Notification.query.filter_by(id_user_request=userData.email).with_entities(Notification.id_user_request, Notification.id_notification, Notification.id_simulation, Notification.date, Notification.finished, Notification.readed).all()
		
		response = jsonify(allNotifications, '0')
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response

	else:
		response = jsonify("GET NOTIFICATIONS ERROR", 1)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response


	

#---------------------CHANGE PASSWORD-------------------------------------------------------------    		
    		
@app.route('/changePassword', methods=['GET', 'POST'])
def changePassword():

	
		token = request.args.get('token')
		new_pass = request.args.get('new_password')
		old_pass = request.args.get('old_password')

		if db.session.query(db.exists().where(Users.security_token==token)).scalar():
		
			if db.session.query(db.exists().where(Users.password==old_pass)).scalar():

				userChangePassword = Users.query.filter_by(security_token=token).first()
				userChangePassword.password = new_pass 
				db.session.merge(userChangePassword)

				db.session.commit()
				
				response = jsonify("CHANGE PASSWORD OK", 0)
				response.headers.add("Access-Control-Allow-Origin", "*")
				return response
				
			else:
			
				response = jsonify("CHANGE PASSWORD ERROR", 1)
				response.headers.add("Access-Control-Allow-Origin", "*")
				return response
			
		
		else:
			
			response = jsonify("CHANGE PASSWORD ERROR", 1)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response

	






#---------------------GET DATA USER-------------------------------------------------------------    		
    		
@app.route('/getDataUser', methods=['GET', 'POST'])
def getDataUser():
    
	try:
		token_request = request.args.get('token')

		if db.session.query(db.exists().where(Users.security_token==token_request)).scalar(): #QUERY TOKEN

			userData = Users.query.filter_by(security_token=request.args.get('token')).first()
			response = jsonify({"email": userData.email, "credits" : userData.credits, "error":0, "password": userData.password })
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			
		else:
			response = jsonify("GET USER DATA ERROR", 1)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			
	except Exception:	
		

		response = jsonify("ERROR", 2)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response

#---------------------DELETE SIMULATION-------------------------------------------------------------    		
    		
@app.route('/deleteSimulation', methods=['GET', 'POST'])
def deleteSimulation():

   

		try:
			simulationData = Simulations.query.filter_by(id_simulation=request.args.get('id_simulation_especific')).first()
			db.session.delete(simulationData)
			db.session.commit()
			response = jsonify("DELETE SIMULATION OK", 0)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			
		except Exception:	
		
			response = jsonify("ERROR", 1)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
		

	
    
#---------------------DELETE USER------------------------------------------------------------    		
    		
@app.route('/deleteUser', methods=['GET', 'POST'])
def deleteUser():
	if Users.query.filter_by(security_token=request.args.get('token')).first():

		try:
			userData = Users.query.filter_by(security_token=request.args.get('token')).first()
			db.session.delete(userData)
			db.session.commit()
			response = jsonify("DELETE USER OK", 0)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
		except Exception:
			return "DELETE USER ERROR"

	else:
		return 'NO USER WITH THIS ID'
    
#---------------------DELETE USER------------------------------------------------------------    		
    		
@app.route('/deletePatient', methods=['GET', 'POST'])
def deletePatient():

    if Patients.query.filter_by(id_patient=request.args.get('id_patient')).first():
        
        try:
            patientData = Patients.query.filter_by(id_patient=request.args.get('id_patient')).first()
            db.session.delete(patientData)
            db.session.commit()
            return "DELETE PATIENT OK"
        except Exception:
            return "DELETE PATIENT ERROR"

    else:
        return 'NO PATIENT WITH THIS ID'
    
#---------------------DELETE PDF FILES------------------------------------------------------------    		
    		
@app.route('/deletePdf', methods=['GET', 'POST'])
def deletePdf():
    
    if Pdffiles.query.filter_by(id_simulation=request.args.get('id_simulation')).first:
        
        try:
            pdfData = Pdffiles.query.filter_by(id_simulation=request.args.get('id_simulation'))
            db.session.delete(pdfData)
            db.session.commit()
            return "DELETE PDF OK"
        except Exception:
            return "DELETE PDF ERROR"

    else:
        return 'NO PDF WITH THIS ID'
    
#---------------------LOAD CREDITS------------------------------------------------------------    		
    		
@app.route('/loadCredits', methods=['GET', 'POST'])
def loadCredits():

    try:

        newCredits = Premium(request.args.get('credits'))
        userLoadCredits = Users.query.filter_by(email=request.args.get('email')).first()
        userLoadCredits.credits = newCredits
        db.session.merge(userLoadCredits)
        db.session.commit()
        

        return 'LOAD CREDITS OK'

    except Exception:
            
        return 'LOAD CREDITS ERROR'
        
        
#---------------------CHANGE PATIENT DATA-------------------------------------------------------------

@app.route('/changePatientData', methods=['GET', 'POST'])

def changePatientData():

	
	if db.session.query(db.exists().where(Patients.id_patient==request.args.get('id_patient_editor'))).scalar(): 
		patientEdited = Patients.query.filter_by(id_patient=request.args.get('id_patient_editor')).first()
		logging.debug(patientEdited.name)
		

		patientEdited.name = request.args.get('first_name_patient_editor')
		
		patientEdited.birth = request.args.get('birth_patient_editor')
		#patientEdited.last_checking = request.args.get('last_visit_patient_editor')
		#patientEdited.first_visit = request.args.get('first_visit_patient_editor')
		patientEdited.contact = request.args.get('contact_patient_editor')
		patientEdited.last_name = request.args.get('last_name_patient_editor')
		patientEdited.number_visits = request.args.get('total_visit_patient_editor')
		patientEdited.address = request.args.get('address_patient_editor')
		patientEdited.notes = request.args.get('notes_patient_editor')
		if(request.args.get('genre_patient_editor')=='man'):
			patientEdited.genre = False;
		else:
			patientEdited.genre = True;
		
			
		db.session.merge(patientEdited)
		db.session.commit()
		
		response = jsonify("CHANGE PATIENT DATA OK", 0)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
			
	else:
		response = jsonify("CHANGE PATIENT DATA OK", 1)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
			
#---------------------SEND NEW REQUEST-------------------------------------------------------------
	
      
@app.route('/sendRequest', methods=['GET', 'POST'])

def sendRequest():

	
	try:
		email = request.args.get('email_request')
		name = request.args.get('name_request')
		message_request = request.args.get('message_request')
		checked = 0
		#

		record = Requests(request.args.get('email_request'), request.args.get('message_request'), datetime.now(), checked)
		db.session.add(record)
		db.session.commit()

		message = Message("New request", sender = app.config ["MAIL_USERNAME"], recipients = ["info@indominus.eu"])
		msn = "New message. From: " + email + ". Message: " + message_request 
		message.body = msn
		mail.send(message)
		#

		response = jsonify( email, 0)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
	
	
	except Exception:
		    
		response = jsonify("ERROR", 1)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
	



	
#--------------------GET CREDITS--------------------------------------------------------------
    		
@app.route('/getCredits', methods=['GET', 'POST'])
def getCredits():

	
		token = request.args.get('token')
		credits_to_buy = request.args.get('credits_to_buy')


		if db.session.query(db.exists().where(Users.security_token==token)).scalar():

			userBuyer = Users.query.filter_by(security_token=token).first()
			
			
			if db.session.query(db.exists().where(Premium.id_user==userBuyer.email)).scalar():
			
			
				response = jsonify("'USER ARE PREMIUM'", 3)
				response.headers.add("Access-Control-Allow-Origin", "*")
				return response
				
			else:
			
				count = int(userBuyer.credits) + int(credits_to_buy)
				userBuyer.credits = count
				db.session.merge(userBuyer)

				db.session.commit()
				
				response = jsonify("'PURCHASE OK'", 0)
				response.headers.add("Access-Control-Allow-Origin", "*")
				return response
				
			
		else:
			
			response = jsonify("'PURCHASE ERROR'", 1)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			

#---------------------GET PREMIUM-------------------------------------------------------------    		
    		
@app.route('/getPremium', methods=['GET', 'POST'])
def getpremium():

	try:

		token = request.args.get('token')

		if db.session.query(db.exists().where(Users.security_token==token)).scalar():
				
			buyerPremium = Users.query.filter_by(security_token=token).first()
			
			if db.session.query(db.exists().where(Premium.id_user==buyerPremium.email)).scalar():
			
			
				response = jsonify("'USER ARE PREMIUM'", 3)
				response.headers.add("Access-Control-Allow-Origin", "*")
				return response
				
			else:
			
				record = Premium(buyerPremium.email)
				db.session.add(record)
				db.session.commit()

				response = jsonify("'GET PREMIUM OK'", 0)
				response.headers.add("Access-Control-Allow-Origin", "*")
				return response

	except Exception:

		response = jsonify("'GET PREMIUM ERROR'", 1)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
		
#--------------------NOTIFICATION READED--------------------------------------------------------------
    		
@app.route('/notificationReaded', methods=['GET', 'POST'])
def notificationReaded():

	
		token = request.args.get('token')
		notification_id = request.args.get('notification_id')
	
		notificationReaded = Notification.query.filter_by(id_notification=notification_id).first()
		
		notificationReaded.readed = 1
		db.session.merge(notificationReaded)

		db.session.commit()
		
		response = jsonify("'READED'", 0)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
				
			
#---------------------GET PREMIUM-------------------------------------------------------------    		
    		
@app.route('/initUser', methods=['GET', 'POST'])
def initUser():



		token = request.args.get('token')

		if db.session.query(db.exists().where(Users.security_token==token)).scalar():
				
			user = Users.query.filter_by(security_token=token).first()
			
			id_user = user.email
			
			user_tokens = user.security_token
			
			n_notifications = Notification.query.filter_by(id_user_request=user.email, readed="false").with_entities(Notification.id_notification).all()
			
			n_simulations = Simulations.query.filter_by(id_user=user.email).with_entities(Simulations.id_simulation).all()
			
			n_patients = Patients.query.filter_by(id_user=user.email).with_entities(Patients.id_patient).all()
			
			premium = db.session.query(db.exists().where(Premium.id_user==user.email)).scalar()
			
			response = jsonify("'OK'", 0, n_patients, n_simulations, n_notifications, user.credits, premium)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
				
		else:
		
			response = jsonify("'ERROR'", 1)
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response

#---------------------SET NOTIFICATIONS-------------------------------------------------------------    		
    		
@app.route('/setNotification', methods=['GET', 'POST'])

def setNotification():

	
		id_notification = request.args.get('id_notification')
		id_user_request = request.args.get('id_user_request')
		id_simulation = request.args.get('id_simulation')
		
		message = Message("Simulation completed", sender = app.config ["MAIL_USERNAME"], recipients = [id_user_request])
		
		
		msn = "The simulation " + id_simulation + " was done."
		
		message.body = msn
		mail.send(message)


		record = Notification(id_user_request , id_simulation, True,  datetime.now(), False, id_notification)
		db.session.add(record)
		db.session.commit()

		response = jsonify(0)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
		
#http://127.0.0.1:5000/setNotification?id_notification=N-018&id_user_request=daniel.sevilla@indominus.eu&id_simulation=S-010		


#---------------------ADD NEW PATIENT -------------------------------------------------------------    		
    		
@app.route('/addNewPatient', methods=['GET', 'POST'])

def addNewPatient():
		

	id_patient_pre = len(Patients.query.all())
	id_patient = id_patient_pre + 1;



	if ( len(str(id_patient))==6 ):
		id_patient = str(id_patient);
	if(len(str(id_patient))==5):
		id_patient = "0" + str(id_patient);
	if(len(str(id_patient))==4):
		id_patient = "00" + str(id_patient);
	if ( len(str(id_patient))==3 ):
		id_patient = "000" + str(id_patient);
	if(len(str(id_patient))==2):
		id_patient = "0000" + str(id_patient);
	if(len(str(id_patient))==1):
		id_patient = "00000" + str(id_patient);

	first_name_patient_editor = request.args.get('first_name_patient_editor')
	last_name_patient_editor = request.args.get('last_name_patient_editor')

	nullDatetime = datetime.fromtimestamp(int("1284105682"))

	if(request.args.get('birth_patient_editor')!='undefined'):
		birth_patient_editor = request.args.get('birth_patient_editor');
	else:
		birth_patient_editor = nullDatetime;

	if(request.args.get('address_patient_editor')!='undefined'):
		address_patient_editor = request.args.get('address_patient_editor');
	else:
		address_patient_editor = "Null";

	if(request.args.get('contact_patient_editor')!='undefined'):
		contact_patient_editor = request.args.get('contact_patient_editor');
	else:
		contact_patient_editor = "Null";

	if(request.args.get('first_visit_patient_editor')!='undefined'):
		first_visit_patient_editor = request.args.get('first_visit_patient_editor');
	else:
		first_visit_patient_editor = nullDatetime;

	if(request.args.get('last_visit_patient_editor')!='undefined'):
		last_visit_patient_editor = request.args.get('last_visit_patient_editor');
	else:
		last_visit_patient_editor = nullDatetime;

	if(request.args.get('total_visit_patient_editor')!='undefined'):
		total_visit_patient_editor = request.args.get('total_visit_patient_editor');
	else:
		total_visit_patient_editor = 0;

	if(request.args.get('notes_patient_editor')!='undefined'):
		notes_patient_editor = request.args.get('notes_patient_editor');
	else:
		notes_patient_editor = "";

	if(request.args.get('genre_patient_editor')=='woman'):

		genre_patient_editor = True;

	else:

		genre_patient_editor = False;


	img_ask = False;
	img_url =  bytearray(b'\x00\x0F')

	allPatients = Patients.query.all()

	record = Patients(id_patient, first_name_patient_editor, request.args.get('id_user'), birth_patient_editor, last_visit_patient_editor, last_name_patient_editor, total_visit_patient_editor, genre_patient_editor, address_patient_editor, first_visit_patient_editor, notes_patient_editor,  contact_patient_editor, img_ask, img_url)
	db.session.add(record)
	db.session.commit()

	response = jsonify("'ADD NEW PATIENT OKEY'", 0)
	response.headers.add("Access-Control-Allow-Origin", "*")
	return response

	
		
		
#http://127.0.0.1:5000/setNotification?id_notification=N-018&id_user_request=daniel.sevilla@indominus.eu&id_simulation=S-010		

#---------------------LOAD DICOM-------------------------------------------------------------

@app.route('/loadDicom', methods=['GET', 'POST'])

def loadDicom():

	
	patient_name = "test1"
	
	# GENERATE KEY FOR SAMPLES
	
	id_sample_pre = len(Samples.query.all())
	id_sample = id_sample_pre + 1;
	
	if ( len(str(id_sample))==3 ):
		id_sample = str(id_sample);
	if(len(str(id_sample))==2):
		id_sample = "0" + str(id_sample);
	if(len(str(id_sample))==1):
		id_sample = "00" + str(id_sample);
	
	# UPLOAD DATA TO DATA BASE
	
	name = patient_name + "-" + id_sample
	date = datetime.now()
	
	
	
	response = jsonify("Uploaded Sample", id_sample, name, date)
	response.headers.add("Access-Control-Allow-Origin", "*")
	return response
		
		
		
		#---------------------GET ALL SIMULATIONS FROM ONE PATIENT-------------------------------------------------------------    		
    		
@app.route('/getSimulationsOfPatient', methods=['GET', 'POST'])
def getSimulationsOfPatient():

	
		patient_id = request.args.get('id_patient')

		allSimulations = Simulations.query.filter_by(id_patient=patient_id).all()
		
		
		all_simulations = [{'id_simulation':simulation.id_simulation, 'file_simulation':simulation.file_simulation, 'id_patient':simulation.id_patient, 'file_model':simulation.file_model, 'id_user':simulation.id_user, 'completed':simulation.completed} for simulation in allSimulations]
		
		response = jsonify(all_simulations, 0)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
			
		

	
#---------------------SAVE PERIDONTAL PROBE------------------------------------------------------------    		
    		
@app.route('/peridontalUpload', methods=['GET', 'POST'])
def peridontalUpload():

	
	pic = request.files['image']
	
	
	if not pic:
		response = jsonify( 'Pic stored')
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response
		
		
	mimetype = pic.mimetype
	

	patient = request.form['patient']
	
	patientProber = Patients.query.filter_by(id_patient=patient).first()
		
	patientProber.image_ask = True
	patientProber.image_url = pic.read()

	db.session.merge(patientProber)

	db.session.commit()

	response = jsonify( 'Pic stored')
	response.headers.add("Access-Control-Allow-Origin", "*")
	return response
		


		
		
		#https://kb.objectrocket.com/postgresql/upload-a-file-to-postgres-with-python-1112
		
		
#---------------------get PERIDONTAL PROBE------------------------------------------------------------    		
    		
@app.route('/getImage', methods=['GET', 'POST'])
def getImage():

	patient = request.args.get('patient')
	patientProber = Patients.query.filter_by(id_patient=patient).first()
		
	image_binary = patientProber.image_url

	return Response(image_binary, mimetype='image/png')
	



	
	
@app.route('/delete_login', methods=['GET', 'POST'])
def delete_login():


	

		emailPost = request.args.get('email')
		security_token = random.randint(0, 99999999999999)

    
		if db.session.query(db.exists().where(Users.email == emailPost)).scalar(): #QUERY EMAIL & PASSWORD
			
			time.sleep(10)
			
			userloged = Users.query.filter_by(email=emailPost).first()
		
			secretNumber = 0
			userloged.security_token = secretNumber
			db.session.merge(userloged)
			db.session.commit()
			
			
			response = jsonify( 'DELETED TOKEN')
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
		else:
		
			response = jsonify( 'no DELETED TOKEN')
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response	
			
@app.route('/delete_login_out', methods=['GET', 'POST'])
def delete_login_out():

		emailPost = request.args.get('email')
		security_token = random.randint(0, 99999999999999)

    
		if db.session.query(db.exists().where(Users.email == emailPost)).scalar(): #QUERY EMAIL & PASSWORD


			userloged = Users.query.filter_by(email=emailPost).first()

			secretNumber = 0
			userloged.security_token = secretNumber
			db.session.merge(userloged)
			db.session.commit()

			response = jsonify( 'DELETED TOKEN')
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response
			
		else:
		
			response = jsonify( 'no DELETED TOKEN')
			response.headers.add("Access-Control-Allow-Origin", "*")
			return response		

		
	
app.run(host='0.0.0.0', port='5000')	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
