import { Component, HostListener } from '@angular/core';
import { NotificationService } from './notification-toaster.service'
import { DentalfemServiceService } from './services/dentalfem-service.service';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from "ngx-spinner";
import {
  IPayPalConfig,
  ICreateOrderRequest 
} from 'ngx-paypal';

import { HttpClient } from '@angular/common/http';

declare var dwv: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

 
  public payPalConfig ? : IPayPalConfig;
  response: any;
  email: any;
  emailLog: any;
  passwordLog: any;
  password: any;
  name: any;
  new_password: any;
  nextStep: any;
  logged: boolean;
  showSimulations: boolean = false;
  showPatients: boolean = false;
  showUserData: boolean = false;
  simulations_list: any[] | undefined;
  patients_list: any[] | undefined;
  data_list: any[] | undefined;
  notification_list: any[] | undefined;
  userCredits: any;
  userDataEmail: any;
  old_password: any;
  authModal = true;
  loadDicomModal = false;
  info_logged_body: any = "DentalFEM digital dentistry tool allows for   data driven surgical procedures based on accurate   mathematical models solved with finit element method (FEM)";
  info_logged_title: any = "Welcome to DentalFEM";
  modalEditPatients = false;
  first_name_patient_editor: any;
  last_name_patient_editor: any;
  genre_patient_editor: any;
  birth_patient_editor: any;
  address_patient_editor: any;
  contact_patient_editor: any;
  first_visit_patient_editor: any;
  last_visit_patient_editor: any;
  total_visit_patient_editor: any;
  notes_patient_editor: any;
  id_patient_editor: any;
  first_name_patient_new: any;
  last_name_patient_new: any;
  genre_patient_new: any;
  birth_patient_new: any;
  address_patient_new: any;
  contact_patient_new: any;
  first_visit_patient_new: any;
  last_visit_patient_new: any;
  total_visit_patient_new: any;
  notes_patient_new: any;
  id_patient_new: any;
  aboutModal = false;
  contactModal = false;
  creditsModal = false;
  notificationModal = false;
  countSimulations : any;
  countPatients: any;
  checkbox_value: any;
  name_request: any;
  email_request: any;
  message_request: any;
  id_patient_simulation: any;
  input_file_dicom: any;
  title = 'toaster-not';
  checkRegisterForm: any;
  sendRequestAccept = false;
  credits_to_buy = 0;
  showDeleteAccept = false;
  id_simulation_especific: any;
  showOneNotification = false;
  notification_id: any;
  notification_date: any;
  notification_simulation: any;
  n_notifications: any;
  n_simulations: any;
  n_patients: any;  
  n_credits: any;
  premium_user: any;
  dicom_loaded: any;
  patients_name_list: Array<string> = [];
  withPatients: any;
  addPatientModal = false;
  message: any;
  privacityModal = false;
  protocolModal = false;
  anonymizeModal = false;
  anonymizeAnswerVar: any;
  anonymizerChecked = true;
  checkboxanonymizer: any;
  isChecked = true;
  uploadedFiles: Array<File> = [];
  uploadedFilesAnonymized: any;
  uploadImageProbe = false;
  peridontal_img = false;
  modalBePremium = false;
  showSuccess: boolean = false;
  showCancel: boolean= false;
  showError: boolean= false;
  resetStatus: any;
  modalGetCredits = false;
  contactModalLogged = false;
  message_request_logged: any;
  name_request_logged: any;
  router: any;

  constructor(private http: HttpClient, public spinner: NgxSpinnerService,public service: DentalfemServiceService, private cookieService: CookieService, private notifyService : NotificationService) {

    this.logged = false;
    this.userDataEmail = cookieService.get('email');
    this.checkRegisterForm = false;
    
    
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 2000);
   
  }

 
  /// NGONINIT. RUNNING WHEN THE PAGE RESTART.

  ngOnInit(){
      this.authModal = false;
      if(this.cookieService.get("token")){

      this.authModal = false;
      this.logged = true;

      this.service.initUser(this.cookieService.get("token")).subscribe( data => {
        if(data[1]==0){
          this.n_notifications = data[4].length;
          this.n_simulations = data[3].length;
          this.n_patients =  data[2].length;
          this.n_credits = Number(data[5]);
          this.premium_user = data[6];
        }
        else{
          this.logged = false;
        }
        if(this.n_patients>0){
          this.withPatients = true;
        }
        else{
          this.withPatients = false;
        }
        
    });

   



    }

    this.payPalConfig = {
      currency: 'EUR',
      clientId: 'sb',
      createOrderOnClient: (data) => < ICreateOrderRequest > {
          intent: 'CAPTURE',
          purchase_units: [{
              amount: {
                  currency_code: 'EUR',
                  value: '9.99',
                  breakdown: {
                      item_total: {
                          currency_code: 'EUR',
                          value: '9.99'
                      }
                  }
              },
              items: [{
                  name: 'Enterprise Subscription',
                  quantity: '1',
                  category: 'DIGITAL_GOODS',
                  unit_amount: {
                      currency_code: 'EUR',
                      value: '9.99',
                  },
              }]
          }]
      },
      advanced: {
          commit: 'true'
      },
      style: {
          label: 'paypal',
          layout: 'vertical'
      },
      onApprove: (data, actions) => {
          console.log('onApprove - transaction was approved, but not authorized', data, actions);
          actions.order.get().then((details: any) => {
              console.log('onApprove - you can get full order details inside onApprove: ', details);
          });

      },
      onClientAuthorization: (data) => {
          console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
          this.showSuccess = true;
      },
      onCancel: (data, actions) => {
          console.log('OnCancel', data, actions);
          this.showCancel = true;

      },
      onError: err => {
          console.log('OnError', err);
          this.showError = true;
      },
      onClick: (data, actions) => {
          console.log('onClick', data, actions);
          this.resetStatus();
      }
  };


 


  }

  


  ////// CHECK VALUE REGISTER ///////////////////
  ///// Function that check if the checkbox of privacy are checked

  checkValueRegister(){
    if(this.checkRegisterForm){
      this.checkRegisterForm=false
    }
    else{
      this.checkRegisterForm=true
    }
  }

  //// LOGIN USER //////////////////

  login(){
    //this.response =  this.service.logIn(this.email, this.password);
    if(this.emailLog==null || this.passwordLog==null){
      this.showToasterWarning("Login fields are empty", "");
    }

    else{
      this.service.login(this.emailLog, this.passwordLog).subscribe( data => {
        if(data[1]==0){
          this.showToasterSuccess("Login sucessfull", "");
          this.cookieService.set('token', data[0] );
          this.cookieService.set('email', data[2] );
          this.userDataEmail = this.cookieService.get('email');
          this.service.initUser(this.cookieService.get("token")).subscribe( data => {
            if(data[1]==0){
              this.n_notifications = data[4].length;
              this.n_simulations = data[3].length;
              this.n_patients =  data[2].length;
              this.n_credits = Number(data[5]);
              this.premium_user = data[6];
              
              
             
            }
            else{
              
            }});
          
          window.location.reload();
          this.authModal = false;
          this.logged = true;
          this.ngOnInit()
          

        }
        else if(data[1]==4){
         
          this.cookieService.set('token', data[0] );
          this.cookieService.set('email', data[2] );
          this.userDataEmail = this.cookieService.get('email');
          this.service.initUser(this.cookieService.get("token")).subscribe( data => {
            if(data[1]==0){
              this.n_notifications = data[4].length;
              this.n_simulations = data[3].length;
              this.n_patients =  data[2].length;
              this.n_credits = Number(data[5]);
              this.premium_user = data[6];
              
              
             
            }
            else{
              
            }});
          
          window.location.reload();
          this.authModal = false;
          this.logged = true;
          this.ngOnInit()
          this.showToasterWarning( "Close the other session before starting", "Another session exists");
        }
        else{
          this.cookieService.delete('token');
          this.cookieService.delete('email');
          this.showToasterError("Incorrect email or password", "");
        }
        
    });
    }  
  }

  //// SIGN IN NEW USER /////////////

  signin(){
    //this.response =  this.service.logIn(this.email, this.password);
    if(this.email==null || this.password==null || this.new_password==null){
      this.showToasterWarning("New user fields are empty", "");
    }
    else if(this.password!=this.new_password){
      this.showToasterError("Passwords are not equal", "");
    }
    else if(!this.checkRegisterForm){
      this.showToasterWarning("You must accept the privacy policy", "");
    }
    else{
      this.service.signin(this.email, this.password, this.name, this.new_password).subscribe( data => {
        if(data[1]==0){
          this.showToasterSuccess("Registration sucessfull", "");
          this.logged = true;
          this.cookieService.set('token', data[0]);
          this.cookieService.set('email', data[2] );
          this.userDataEmail = this.cookieService.get('email');
          this.authModal = false;
          
          this.service.initUser(this.cookieService.get("token")).subscribe( data => {
            if(data[1]==0){
              this.n_notifications = data[4].length;
              this.n_simulations = data[3].length;
              this.n_patients =  data[2].length;
              this.n_credits = Number(data[5]);
              this.premium_user = data[6];
              if(this.n_patients>=1){
                console.log("Se habilita todo")
                this.withPatients = true;
              } else{
                this.withPatients = false;
                console.log("Se deshabilita todo")
              }
            }
            else{
              
            }});

         
          
          
        }
        else{
          this.cookieService.delete('token');
          this.cookieService.delete('email');
          this.showToasterError("This email already exists on the platform", "");
        }
        
      
      });
    }
  }

  //// LOG OUT /////////

  logOut(){
    this.service.deleteLoginOut(this.cookieService.get('email')).subscribe( data => {
      if(data==0){
        
            
           
          }
          else{
            
          }});
    this.cookieService.delete('token');
    this.cookieService.delete('email');
    this.logged = false;
    this.showUserData = false;
    
    
  }

  ////OPEN AUTH MODAL ////////////////////

  openAuthModal(){
    this.authModal = true;
  }

  openShowSimulations(){
    this.showSimulations = true;
    this.service.getSimulations(this.cookieService.get("token")).subscribe( data => {
      if(data[1]==0){
        this.simulations_list=data[0]
      }
      else{
        
      }
      
    });
  }

  openShowPatients(){
    this.showPatients = true;
    this.service.getPatients(this.cookieService.get("token")).subscribe( data => {
      if(data[1]==0){
        for(let i = 0; i<data[0].length; i++){
          if(data[0][i].first_visit=="Fri, 10 Sep 2010 10:01:22 GMT"){
            data[0][i].first_visit="No results";
          }
          if(data[0][i].last_checking=="Fri, 10 Sep 2010 10:01:22 GMT"){
            data[0][i].last_checking="No results";
          }
          if(data[0][i].birth=="Fri, 10 Sep 2010 10:01:22 GMT"){
            data[0][i].birth="No results";
          }
          this.patients_list=data[0]
        }
      }
      else{
        
      }
      
    });
  }

  openContactModalLogged(){

    this.contactModalLogged = true;
  }

  closemyModalContactLogged(){

    this.contactModalLogged = false;
  }

  openUserData(){
    
    this.service.getUserData(this.cookieService.get("token")).subscribe( data => {
      if(data.error==0){
        this.userDataEmail=data.email;
        this.userCredits=data.credits;
        this.showUserData = true;
      }
      else{
        
      }
      
    });
  }

  changePatientData(){
    this.service.changePatientData(this.notes_patient_editor, this.id_patient_editor, this.first_name_patient_editor, this.last_name_patient_editor, this.genre_patient_new, this.birth_patient_editor, this.address_patient_editor, this.contact_patient_editor, this.first_visit_patient_editor, this.last_visit_patient_editor, this.total_visit_patient_editor).subscribe( data => {
      if(data){
        this.modalEditPatients=false;
        this.showPatients=false;
        window.location.reload();
      }
      else{
      }
    });
      
  }

  openLoadDicom(){
      this.loadDicomModal = true;
      this.service.getPatients(this.cookieService.get("token")).subscribe( data => {
        if(data[1]==0){
          this.patients_list = data[0]
          ///for (let i = 0; i< data[0].length; i++){
            //this.patients_name_list.push(data[0][i].name + ", " + data[0][i].last_name);  
          //}       
        }
        else{
        }
      });

  }

  changePassword(){
    this.service.changePassword(this.cookieService.get("token"), this.new_password, this.old_password).subscribe( data => {
      if(data[1]==0){
        this.showUserData=false;
      }
      else{
        
      }
      
      
  });
  }

  closemyModalAuth( ){
    this.authModal=false;
  }


  closePatientEditor(){
    this.modalEditPatients=false;
  }

  closemyModalShowSimulations(){
    this.showSimulations=false;
  }

  closemyModalShowPatients(){
    this.showPatients=false;
  }

  closemyModalUserData(){
    this.showUserData=false;
  }

  closemyModalLoadDicom(){
    this.loadDicomModal=false;
  }

  openPatientEditor( editing_patient: any ){
    this.modalEditPatients=true;
    this.id_patient_editor = editing_patient.id_patient 
    this.first_name_patient_editor = editing_patient.name
    this.last_name_patient_editor = editing_patient.last_name
    this.peridontal_img = editing_patient.peridontal
    if(editing_patient.genre==true){
      this.genre_patient_editor = "Woman"
    } else{
      this.genre_patient_editor = "Man"
    }
    
    this.birth_patient_editor = editing_patient.birth
    this.address_patient_editor = editing_patient.address
    this.contact_patient_editor = editing_patient.contact
    this.first_visit_patient_editor = editing_patient.first_visit
    this.last_visit_patient_editor = editing_patient.last_checking
    this.total_visit_patient_editor = editing_patient.number_visits
    this.notes_patient_editor = editing_patient.notes
  }

  deleteUser(){
    this.service.deleteUser(this.cookieService.get("token")).subscribe( data => {
      if(data[1]==0){
        this.cookieService.delete('token');
        this.logged = false;
        this.showUserData = false;
      }
      else{
        
      }});
  }
    
  closemyModalAbout(){
    this.aboutModal = false;
  }

  openModalAbout(){
    this.aboutModal = true;
  }

  closemyModalContact(){
    this.contactModal = false;
  }

  openModalContact(){
    this.contactModal = true;
  }

  closemyModalCredits(){
    this.creditsModal = false;
  }

  openModalCredits(){
    this.creditsModal = true;
  }

  closemyModalNotifications(){
    this.notificationModal = false;
    this.ngOnInit;
    window.location.reload();
  }

  openModalNotification(){
    this.notificationModal = true;
    this.service.getNotification(this.cookieService.get("token")).subscribe( data => {
      if(data[1]==0){

        this.notification_list=data[0]

        
  
      }
      else{
        
      }
      
    });
  }


  loadDicom(){

    
    
    const formData = new FormData();

    
    this.service.loadDicom(this.cookieService.get("token"), this.id_patient_simulation, formData).subscribe( data => {
      if(data[1]==0){
        this.showUserData=false;
      }
      else{
        
      }
      
      
  });
    
}

  buyCredits(){

    this.service.getCredits(this.cookieService.get("token"), this.credits_to_buy).subscribe( data => {
      if(data[1]==0){
        this.creditsModal=false;
        this.showToasterSuccess("The purchase has been successfully completed", "");
        this.ngOnInit;
        window.location.reload();
      }
      else if(data[1]==3){
        this.showToasterWarning("The user acount is premium. It's not neccessary buy credits.", "");
      }
      else{
        this.showToasterError("The purchase has not been successfully completed", "");
      }});

  }


  buyPremium(){

    this.service.getPremium(this.cookieService.get("token")).subscribe( data => {
      if(data[1]==0){
        this.creditsModal=false;
        this.showToasterSuccess("The purchase has been successfully. The account has premium privileges", "");
        this.ngOnInit;
        window.location.reload();
      }
      else if(data[1]==3){
        this.showToasterWarning("The account is already premium.", "");
      }
      else{
        this.showToasterError("The purchase has not been successfully completed", "");
      }});
      
  }


  checkdeleteSimulation(id_simulation_especific: any){
    this.showDeleteAccept = true;
    this.id_simulation_especific = id_simulation_especific
  }

  closeShowDeleteAccept(){
    this.showDeleteAccept = false;
  }

  deleteSimulation(){

    this.service.deleteSimulation(this.id_simulation_especific ).subscribe( data => {
      if(data[1]==0){
        this.showSimulations=false;
        this.showDeleteAccept = false;
        this.showToasterSuccess("The simulation has been deleted", "");
        this.ngOnInit();
        window.location.reload();
      }
      else if(data[1]==1){
        this.showToasterWarning("The simulation has not been deleted", "");
      }
      else{
        this.showToasterError("The simulation has not been deleted", "");
      }});

  }

  showNotificationData(id_notification: any, date: any, id_simulation: any){
    this.showOneNotification=true;
    this.notification_id = id_notification;
    this.notification_date = date;
    this.notification_simulation = id_simulation;
    this.service.notificationReaded(this.notification_id).subscribe( data => {
      if(data[1]==0){
      }
      else{
      }});   
  }

  

  closeShowOneNotification(){
    this.showOneNotification=false;
  }

  openAnonymizedAnswer(value: any){
   
    if(value=="false"){
      this.anonymizeModal = true;
    }
   
  }


  changeAnonymized(){

    this.isChecked = true;
    this.anonymizerChecked = true;
    this.anonymizeModal= false;
  }

  closeAnonymized(){
    this.anonymizeModal= false;
  }

  anonymizedNo(){
    this.anonymizeAnswerVar = false;
    this.anonymizeModal= false;
  }

  ///////////// REQUEST FUNCTIONS ////////////////////////////////////////////////////////////////////////////////////


  acceptSendRequest(){

    this.service.sendRequest(this.name_request, this.email_request, this.message_request).subscribe( data => {
      if(data[1]==0){
        this.showToasterSuccess("Request sended sucessfully", "");
        this.sendRequestAccept = false;
        this.contactModal = false;
      }
      else{
        this.showToasterError("Request could not be sent", "");
        
      }});
    }  

  sendRequest(){
    if(this.name_request == null || this.email_request == null || this.message_request == null){
      this.showToasterWarning("Request fields are empty", "");
    }else{
      this.sendRequestAccept = true;
    }
    
  }

  sendRequestLogged(){
    if(this.name_request_logged == null){
      this.showToasterWarning("Request fields are empty", "");
    }else{
      this.sendRequestAccept = true;
    }
    
  }

  closeRequest(){
      this.sendRequestAccept = false;
  }

  closeAddPatientModal(){
      this.addPatientModal = false;
  }

  openAddPatientModal(){
    this.addPatientModal = true;
  }

  addNewPatient(){
    
    if(this.first_name_patient_new==null || this.last_name_patient_new==null){
      this.showToasterWarning("New user fields are empty", "");
    }
    else{
      this.service.addNewPatient( this.first_name_patient_new, this.last_name_patient_new, this.genre_patient_new, this.birth_patient_new, this.address_patient_new, this.contact_patient_new, this.first_visit_patient_new, this.last_visit_patient_new, this.total_visit_patient_new, this.notes_patient_new, this.cookieService.get("email")).subscribe( data => {
        if(data[1]==0){
          this.showToasterSuccess("Patient added sucessfully", "");
          
          this.addPatientModal = false;
          window.location.reload();
          
        }
        else{
         
          this.showToasterError("Cannot add this new patient", "");
        }
        
      
      });
    }
  }

  openPrivacityModal(){
    this.privacityModal = true;
  }

  closePrivacityModal(){
    this.privacityModal = false;
  }

  openProtocol(){
    this.protocolModal = true;
  }
 
  closeProtocolModal(){
    this.protocolModal = false;
  }

  closeModalBePremium(){
    this.modalBePremium = false;

  }

  openModalBePremium(){
    this.modalBePremium = true;
    
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


 





  //////////////////////////////////////////////////////
  ////           TOASTERS ALETS            /////////////
  //////////////////////////////////////////////////////


  showToasterSuccess(body: any, title: any){
    this.notifyService.showSuccess(body, title)
  }

  showToasterError(body: any, title: any){
      this.notifyService.showError(body, title)
  }

  showToasterInfo(body: any, title: any){
      this.notifyService.showInfo(body, title)
  }

  showToasterWarning(body: any, title: any){
      this.notifyService.showWarning(body, title)
  }

  fileChange(element: any) {

    if(element.target.files[0].type == "application/dicom"){
      this.notifyService.showSuccess("", "Valid file");
      this.uploadedFiles = element.target.files;
      dwv.test.onInputDICOMFile(element);
    }
    else{
      
      this.notifyService.showError("Accepted files: dcm", "Invalid file");
    }
    
   
    
  
    
  }

  upload() {

    if(this.anonymizeAnswerVar==false){
      this.spinner.show();
      let formData = new FormData();
      
      for (var i = 0; i < this.uploadedFiles.length; i++) {
        formData.append("uploads[]", this.uploadedFiles[i], this.uploadedFiles[i].name);
       }
      this.service.uploadDicom(formData);
      setTimeout(() => { 
        
        this.spinner.hide();
        this.showToasterSuccess("Sample successfully uploaded", "");
      }, 65000);}
    else {

      dwv.test.generate();
       
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
      this.loadDicomModal = false;
      this.showToasterSuccess("Sample successfully uploaded", "");
    }, 65000);


      

    }


    
      
    
  }

  onImgSelected(element: any){
    const file:File = element.target.files[0];
    if (file) {
      this.spinner.show();
      let fileName = file.name;
      const formData = new FormData();
      formData.append("image", file);
      formData.append("patient", this.id_patient_editor);
      const upload$ = this.http.post("http://217.76.155.161:5000/peridontalUpload", formData);
      upload$.subscribe();
      this.peridontal_img = true;
     
    }
    
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
      this.showToasterSuccess("Peridontal Image uploaded successfully", "");
    }, 2000);
    

    
  }


  getImg(){
    this.service.getImageProbe(this.id_patient_editor).subscribe( data => {
      if(data==0){
        
      }
      else{
        
      }
      
    });
    
  }

  closemyModalImageProbe(){
    this.uploadImageProbe = false;
    window.location.reload();  }

  openmyModalImageProbe(){
    this.uploadImageProbe = true;
  }

  closeModalGetCredits(){
    this.modalGetCredits = false;
  }

  openModalGetCredits(){
    this.modalGetCredits = true;
  }
  
  
  
}


