import { TestBed } from '@angular/core/testing';

import { NotificationToasterService } from './notification-toaster.service';

describe('NotificationToasterService', () => {
  let service: NotificationToasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotificationToasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
