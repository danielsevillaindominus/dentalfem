import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroWebComponent } from './intro-web.component';

describe('IntroWebComponent', () => {
  let component: IntroWebComponent;
  let fixture: ComponentFixture<IntroWebComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntroWebComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
