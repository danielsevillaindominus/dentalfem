import { TestBed } from '@angular/core/testing';

import { DentalfemServiceService } from './dentalfem-service.service';

describe('DentalfemServiceService', () => {
  let service: DentalfemServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DentalfemServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
