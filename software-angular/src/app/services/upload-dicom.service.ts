import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType, HttpHeaders } from  '@angular/common/http';
import { map } from  'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadDicomService {
  serverUrl: string = "http://127.0.0.1:5000/loadDicom" ;
  constructor(private httpClient: HttpClient) { }

  httpOptionsPost = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json, application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Methods' : 'GET, POST, OPTIONS, PUT',
      'Access-Control-Allow-Headers' : 'Origin, Content-Type, Accept, X-Custom-Header, Upgrade-Insecure-Requests',
      
      
    })
  };

  httpOptionsGet = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json, application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': "https://127.0.0.1:5000/",
    })
  };

  public sendFormData(formData: any) {    
    return this.httpClient.post<any>(this.serverUrl, formData, {
    headers: this.httpOptionsPost.headers,
    reportProgress: true,
    observe: 'events'
  });
  }

}

