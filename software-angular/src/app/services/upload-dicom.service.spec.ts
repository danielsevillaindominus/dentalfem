import { TestBed } from '@angular/core/testing';

import { UploadDicomService } from './upload-dicom.service';


describe('UploadDicomService', () => {
  let service: UploadDicomService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploadDicomService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
