import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DentalfemServiceService {

  rutaApi = "http://217.76.155.161:5000/"
  //rutaApi = "http://127.0.0.1:5000/"
  
  response: any;
  email: any;
  password: any;
  token: any;
  
  
  constructor(private http: HttpClient) { }

  httpOptionsPost = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Methods' : 'GET, POST, OPTIONS',
      'Access-Control-Allow-Headers' : 'Origin, Content-Type, Accept, X-Custom-Header, Upgrade-Insecure-Requests',
    })
  };

  httpOptionsGet = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "https://217.76.155.161:5000/",
    })
  };


  login(email: string, password: string): Observable<any>{

    return this.http.get(`${this.rutaApi}login?email=${email}&password=${password}`);

  }

  signin(email: string, password: string, new_password: string, name: string): Observable<any>{

    return this.http.get(`${this.rutaApi}signin?email=${email}&password=${password}&new_password=${name}&name=${new_password}`);

  }

  getSimulations(token: string): Observable<any>{
    return this.http.get(`${this.rutaApi}getSimulations?token=${token}`);
  }

  getPatients(token: string): Observable<any>{
    return this.http.get(`${this.rutaApi}getPatients?token=${token}`);
  }

  getUserData(token: string): Observable<any>{
    return this.http.get(`${this.rutaApi}getDataUser?token=${token}`);
  }

  changePassword(token: string, new_password: string, old_password: string): Observable<any>{
    return this.http.get(`${this.rutaApi}changePassword?token=${token}&new_password=${new_password}&old_password=${old_password}`);
  }

  changePatientData(notes_patient_editor: string, id_patient_editor: string, first_name_patient_editor: string, last_name_patient_editor: string, genre_patient_editor: any, birth_patient_editor: Date, address_patient_editor: string, contact_patient_editor: string, first_visit_patient_editor: Date, last_visit_patient_editor: Date,total_visit_patient_editor: string){
    return this.http.get(`${this.rutaApi}changePatientData?first_name_patient_editor=${first_name_patient_editor}&last_name_patient_editor=${last_name_patient_editor}&genre_patient_editor=${genre_patient_editor}&birth_patient_editor=${birth_patient_editor}&address_patient_editor=${address_patient_editor}&contact_patient_editor=${contact_patient_editor}&first_visit_patient_editor=${first_visit_patient_editor}    &last_visit_patient_editor=${last_visit_patient_editor}&total_visit_patient_editor=${total_visit_patient_editor}&id_patient_editor=${id_patient_editor}&notes_patient_editor=${notes_patient_editor}`);
  }

  deleteUser(token: string): Observable<any>{
    return this.http.get(`${this.rutaApi}deleteUser?token=${token}`);
  }

  sendRequest(name_request: string, email_request: string, message_request: string): Observable<any>{
    return this.http.get(`${this.rutaApi}sendRequest?name_request=${name_request}&email_request=${email_request}&message_request=${message_request}`);
  }

  getNotification(token: string): Observable<any>{
    return this.http.get(`${this.rutaApi}getNotifications?token=${token}`);
  }

  loadDicom(token: string, id_patient_simulation: string, input_file_dicom: FormData): Observable<any>{
    return this.http.get(`${this.rutaApi}loadDicom?token=${token}&id_patient_simulation=${id_patient_simulation}&input_file_dicom=${input_file_dicom}`);
  }

  getCredits(token: string, credits_to_buy: any): Observable<any>{

    return this.http.get(`${this.rutaApi}getCredits?token=${token}&credits_to_buy=${credits_to_buy}`);
 
  }

  getPremium(token: string): Observable<any>{

    return this.http.get(`${this.rutaApi}getPremium?token=${token}`);
 
  }

  deleteSimulation(id_simulation_especific: any): Observable<any>{

    return this.http.get(`${this.rutaApi}deleteSimulation?id_simulation_especific=${id_simulation_especific}`);

  }

  notificationReaded(notification_id: any): Observable<any>{

    return this.http.get(`${this.rutaApi}notificationReaded?notification_id=${notification_id}`);

  }

  initUser(token: string): Observable<any>{

    return this.http.get(`${this.rutaApi}initUser?token=${token}`);
 
  }

  addNewPatient( first_name_patient_editor: any, last_name_patient_editor: any, genre_patient_editor: any, birth_patient_editor: any, address_patient_editor: any, contact_patient_editor: any, first_visit_patient_editor: any, last_visit_patient_editor: any, total_visit_patient_editor: any, notes_patient_editor: any, id_user: any): Observable<any>{
    return this.http.get(`${this.rutaApi}addNewPatient?first_name_patient_editor=${first_name_patient_editor}&last_name_patient_editor=${last_name_patient_editor}&birth_patient_editor=${birth_patient_editor}&address_patient_editor=${address_patient_editor}&contact_patient_editor=${contact_patient_editor}&first_visit_patient_editor=${first_visit_patient_editor}&last_visit_patient_editor=${last_visit_patient_editor}&total_visit_patient_editor=${total_visit_patient_editor}&notes_patient_editor=${notes_patient_editor}&genre_patient_editor=${genre_patient_editor}&id_user=${id_user}`);
  }

  uploadDicom( FormData: any  ){
    this.http.post('http://217.76.155.161:3200/api/upload', FormData)
    .subscribe((response) => {
        
    }) 
   
  }

  getImageProbe(patient: any){
    return this.http.get(`${this.rutaApi}getImage?patient=${patient}`);
  }

  deleteLogin(email: any){
    return this.http.get(`${this.rutaApi}delete_login?email=${email}`);
  }

  deleteLoginOut(email: any){
    return this.http.get(`${this.rutaApi}delete_login_out?email=${email}`);
  }


}

