export class Users {
    constructor(
          public email: string,
          public password: string,
          public credits: number,
          security_token: string
      ) { }
  }