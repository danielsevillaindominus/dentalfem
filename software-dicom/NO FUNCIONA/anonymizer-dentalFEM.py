from flask import Flask, render_template, request, flash, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
import random
import datetime
from sqlalchemy.sql.functions import user
from datetime import date, time, datetime
import logging
import tempfile
import pydicom
from pydicom.data import get_testdata_file


app = Flask(__name__)

#COLORES
#https://javier.xyz/cohesive-colors/?src=594F4F,547980,45ADA8,9DE0AD,E5FCC2&overlay=FF9C00&intensity=0

#DATA BASE CONECTION
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://daniel.sevilla:Indominus_2021*;@localhost/dentalfemdb'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'secret string'
db = SQLAlchemy(app)


#CLASS THAT LOADS SIMULATIONS
class Simulations(db.Model):
    __tablename__ = 'simulations'
    id_simulation = db.Column(db.String, primary_key=True)
    file_simulation = db.Column(db.Integer)
    file_model = db.Column(db.Integer)
    completed = db.Column(db.Boolean, default=False, nullable=False)
    id_user = db.Column(db.String)
    id_patient = db.Column(db.String)
    

    def __init__(self, id_simulation, file_simulation, file_model, completed, id_user, id_patient):
        self.id_simulation = id_simulation
        self.file_simulation = file_simulation
        self.file_model = file_model
        self.completed = completed
        self.id_user = id_user
        self.id_patient = id_patient

#CLASS THAT LOADS USERS
class Users(db.Model):
    __tablename__ = 'users'
    email = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    credits = db.Column(db.Integer)
    security_token = db.Column(db.String)
    

    def __init__(self, email, password, credits, security_token):
        self.email = email
        self.password = password
        self.credits = credits
        self.security_token = security_token

#CLASS THAT LOADS PATIENTS
class Patients(db.Model):
	__tablename__ = 'patients'
	id_patient = db.Column(db.String, primary_key=True)
	name = db.Column(db.String)
	id_user = db.Column(db.String)
	birth = db.Column(db.DateTime)
	last_checking = db.Column(db.DateTime)
	last_name = db.Column(db.String)
	number_visits = db.Column(db.Integer) 
	genre = db.Column(db.Boolean, default=False, nullable=False)
	address = db.Column(db.String)
	first_visit = db.Column(db.DateTime)
	notes = db.Column(db.String)
	contact = db.Column(db.String)


	def __init__(self, id_patient, name, id_user, birth, last_checking, last_name, number_visits, genre, address, first_visit, notes):
		self.id_patient = id_patient
		self.name = name
		self.id_user = id_user
		self.birth = birth
		self.last_checking = last_checking
		self.last_name = last_name
		self.number_visits = number_visits
		self.genre = genre
		self.address = address
		self.first_visit = first_visit
		self.notes = notes
		self.contact = contact

#CLASS THAT LOADS PREMIUM
class Premium(db.Model):
    __tablename__ = 'premium'
    id_user = db.Column(db.String, primary_key=True)
    

    def __init__(self, id_user):
        self.id_user = id_user

#CLASS THAT LOADS pdfs
class Pdffiles(db.Model):
    __tablename__ = 'pdfFiles'
    id_simulation = db.Column(db.String, primary_key=True)
    datecreated = db.Column(db.String)
    id_patients = db.Column(db.String)
    

    def __init__(self, id_simulation, datecreated, id_patients):
        self.id_simulation = id_simulation
        self.datecreated = datecreated
        self.id_patients = id_patients
        
#CLASS THAT LOADS requests
class Requests(db.Model):
    __tablename__ = 'requests'
    id_user_request = db.Column(db.String, primary_key=True)
    body = db.Column(db.String)
    date_sended = db.Column(db.DateTime)
    checked = db.Column(db.Boolean, default=False, nullable=False)
    
    def __init__(self, id_user_request, body, date_sended, checked):
        self.id_user_request = id_user_request
        self.body = body
        self.date_sended = date_sended
        self.checked = checked
        
#CLASS THAT LOADS requests
class Notification(db.Model):
    __tablename__ = 'notification'
    id_user_request = db.Column(db.String, primary_key=True)
    id_simulation = db.Column(db.String)
    finished = db.Column(db.Boolean, default=False, nullable=False)
    date = db.Column(db.DateTime)
    
    def __init__(self, id_user_request, id_simulation, finished, date):
        self.id_user_request = id_user_request
        self.id_simulation = id_simulation
        self.finished = finished
        self.date = date

@app.errorhandler(Exception)          
def basic_error(e):          
    return "an error occured: " + str(e)

#---------------------REGISTER-------------------------------------------------------------
#http://127.0.0.1:5000/register?email=daniel@gmail.com&password=1423
@app.route('/anonymizer', methods=['GET', 'POST'])

def anonymizer():

	try:
		filename = get_testdata_file('MR_small.dcm')
		dataset = pydicom.dcmread(filename)

		data_elements = ['PatientID',
				 'PatientBirthDate']
		for de in data_elements:
		    print(dataset.data_element(de))

		###############################################################################
		# We can define a callback function to find all tags corresponding to a person
		# names inside the dataset. We can also define a callback function to remove
		# curves tags.


		def person_names_callback(dataset, data_element):
		    if data_element.VR == "PN":
			data_element.value = "anonymous"


		def curves_callback(dataset, data_element):
		    if data_element.tag.group & 0xFF00 == 0x5000:
			del dataset[data_element.tag]


		###############################################################################
		# We can use the different callback function to iterate through the dataset but
		# also some other tags such that patient ID, etc.

		dataset.PatientID = "id"
		dataset.walk(person_names_callback)
		dataset.walk(curves_callback)

		###############################################################################
		# pydicom allows to remove private tags using ``remove_private_tags`` method

		dataset.remove_private_tags()

		###############################################################################
		# Data elements of type 3 (optional) can be easily deleted using ``del`` or
		# ``delattr``.

		if 'OtherPatientIDs' in dataset:
		    delattr(dataset, 'OtherPatientIDs')

		if 'OtherPatientIDsSequence' in dataset:
		    del dataset.OtherPatientIDsSequence

		###############################################################################
		# For data elements of type 2, this is possible to blank it by assigning a
		# blank string.

		tag = 'PatientBirthDate'
		if tag in dataset:
		    dataset.data_element(tag).value = '19000101'

		##############################################################################
		# Finally, this is possible to store the image

		data_elements = ['PatientID',
				 'PatientBirthDate']
		for de in data_elements:
		    print(dataset.data_element(de))

		output_filename = tempfile.NamedTemporaryFile().name
		dataset.save_as(output_filename)
		
		response = jsonify(security_token, 0, email)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response

	except Exception:

		response = jsonify("ERROR", 2)
		response.headers.add("Access-Control-Allow-Origin", "*")
		return response



if __name__ == '__main__':
    app.run(host="localhost", port=7100, debug=True)
	
